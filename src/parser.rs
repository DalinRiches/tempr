///////////////////////////////////////////////////////////////////////////////
//
//  File: parser.rs
//
//  Original Author: Dalin Riches (driches@ualberta.ca)
//
// Module Documentation -------------------------------------------------------
//!
///////////////////////////////////////////////////////////////////////////////
//  Imports
// ----------------------------------------------------------------------------
use crate::template::Template;
use std::error::Error;
use std::fs::File;
use std::io::prelude::*;
use std::path::{Path, PathBuf};

///////////////////////////////////////////////////////////////////////////////
//  Exported Types
// ----------------------------------------------------------------------------
#[derive(Clone, Debug)]
pub enum TokenCase {
    SnakeCase,
    MacroCase,
    LispCase,
    CamelCase,
    PascalCase,
}

pub struct Token {
    words: Vec<String>,
    case: TokenCase,
}

impl Token {
    fn new(case: TokenCase, words: Vec<String>) -> Self {
        Self {
            words,
            case,
        }
    }

    fn to_string(&self) -> String {
        match self.case {
            TokenCase::PascalCase => String::new(),
            TokenCase::CamelCase => String::new(),
            TokenCase::LispCase => {
                self.words.join("-")
            }
            TokenCase::SnakeCase => {
                self.words.join("_")
            }
            TokenCase::MacroCase => {
                self.words.iter()
                    .map(|word| word.to_uppercase())
                    .collect::<Vec<String>>()
                    .join("_")
            }
        }
    }

    fn case(&self) -> TokenCase {
        self.case.clone()
    }

    fn words(string: &str, case: TokenCase) -> Vec<String> {
        match case {
            TokenCase::PascalCase => {
                Vec::new()
            }
            TokenCase::CamelCase => {
                Vec::new()
            }
            TokenCase::LispCase => {
                string.split("-")
                    .map(|word| { word.to_lowercase() })
                    .collect()
            }
            TokenCase::MacroCase => {
                string.split("_")
                    .map(|word| { word.to_lowercase() })
                    .collect()
            }
            TokenCase::SnakeCase => {
                string.split("_")
                    .map(|word| { word.to_lowercase() })
                    .collect()
            }
        }
    }
}

impl PartialEq for Token {
    fn eq(&self, other: &Self) -> bool {
        self.words == other.words
    }
}

pub struct TokenBuilder {
    default: Option<TokenCase>,
    case: Option<TokenCase>,
}

impl TokenBuilder {
    fn new() -> TokenBuilder {
        Self { 
            default: None,
            case: None,
        }
    }

    fn default_case(mut self, case: TokenCase) -> Self {
        self.default = Some(case);
        self
    }

    fn case(mut self, case: TokenCase) -> Self {
        self.case = Some(case);
        self
    }

    fn create(self, string: &str) -> Token {
        let case: TokenCase;

        // Count capitals, dashs, and underscores
        let (capitals, dashs, underscores) = string.chars()
            .map(|c| { 
                if c.is_uppercase() {
                    (1,0,0)
                } else if c == '_' {
                    (0,1,0)
                } else if c == '-' {
                    (0,0,1)
                } else {
                    (0,0,0)
                }
            })
            .fold((0, 0, 0,), |sum, x| (sum.0 + x.0, sum.1 + x.1, sum.2 + x.2));
        
        // Determine case
        // TODO: There is probably a better way to do this
        if let Some(c) = self.case {
            case = c;
        } else if capitals == string.len() {
            case = TokenCase::MacroCase;
        } else if capitals > 0 {
            if string.chars().next().unwrap().is_uppercase() {
                case = TokenCase::PascalCase;
            } else {
                case = TokenCase::CamelCase;
            }
        } else if dashs > underscores {
            case = TokenCase::LispCase;
        } else if underscores > 0 {
            case = TokenCase::SnakeCase;
        } else {
            if let Some(default_case) = self.default {
                case = default_case;
            } else {
                panic!("Could not determine case for \"{}\"", string);
            }
        }

        // Break into words
        let words = Token::words(string, case.clone());

        Token::new(case, words)
    }
}



pub trait Transaction {
    fn commit(&self) -> Result<(), Box<dyn Error>>;
}

#[derive(Debug, PartialEq, Eq)]
pub struct DirTransaction {
    path: PathBuf,
}

impl Transaction for DirTransaction {
    fn commit(&self) -> Result<(), Box<dyn Error>> {
        std::fs::create_dir(self.path.clone())?;

        Ok(())
    }
}

impl std::fmt::Display for DirTransaction {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self.path)
    }
}

#[derive(Debug, PartialEq, Eq)]
pub struct FileTransaction {
    path: PathBuf,
    text: String,
}

impl Transaction for FileTransaction {
    fn commit(&self) -> Result<(), Box<dyn Error>> {
        let mut file = File::create(self.path.clone())?;
        write!(file, "{}", self.text)?;

        Ok(())
    }
}

impl std::fmt::Display for FileTransaction {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self.path)
    }
}

///////////////////////////////////////////////////////////////////////////////
//  Internal Types
// ----------------------------------------------------------------------------
enum State {
    Prefix,
    Variable,
}

///////////////////////////////////////////////////////////////////////////////
//  Internal Routines
// ----------------------------------------------------------------------------

///////////////////////////////////////////////////////////////////////////////
// External Routines
// ----------------------------------------------------------------------------
/// Parses a template into transactions that need to occur for the template to
/// be applied.
///
/// Returns a tuple of tranaction vectors. One for directory transactions, the
/// other for file transactions. A transaction will be produced and added to
/// the lists if the dir/file exsists within the template directory and does
/// not exsist within the target directory.
pub fn parse(
    src: &Path,
    dest: &Path,
    template: &Template,
) -> (Vec<DirTransaction>, Vec<FileTransaction>) {
    let mut dir_transations: Vec<DirTransaction> = Vec::new();
    let mut file_transations: Vec<FileTransaction> = Vec::new();

    // Iterate over all files/directorys in template path
    // TODO: To many unwraps!
    for entry in src.read_dir().unwrap() {
        let entry = entry.unwrap();
        let filetype = entry.file_type().unwrap();
        let name = entry.file_name().into_string().unwrap();
        
        let mut src_path = src.to_path_buf();
        src_path.push(name.clone());

        let name = _parse(template, &name);

        // Check if file/directory exsists
        let mut dest_path = dest.to_path_buf();
        dest_path.push(name.clone());

        if !dest_path.exists() {
            // File or directory does not exsist so create a transaction
            if filetype.is_dir() {
                // We must recursively parse the directory
                let (mut dirs, mut files) = parse(&src_path.clone(), &dest_path.clone(), template);

                dir_transations.push(DirTransaction { path: dest_path });

                dir_transations.append(&mut dirs);
                file_transations.append(&mut files);
            } else if filetype.is_file() && name != "template.json" {
                // Load the file text and parse it!
                let mut text = String::new();

                {
                    let mut file = File::open(entry.path()).unwrap();
                    file.read_to_string(&mut text).unwrap();
                }

                text = _parse(template, &text);

                file_transations.push(FileTransaction { path: dest_path, text });
            }
        }
    }

    (dir_transations, file_transations)
}

///////////////////////////////////////////////////////////////////////////////
// Internal Routines
// ----------------------------------------------------------------------------
fn _parse<'a>(template: &Template, text: &'a str) -> String {
    let mut text = text.to_string();
    let mut token_stack: String = String::new();
    let mut state: State = State::Prefix;

    let mut start_index: usize = 0;
    let mut end_index: usize;

    let mut changes: Vec<(usize, usize, String)> = Vec::new();

    match template.vars() {
        Some(_) => {}
        None => return text,
    };

    for (index, c) in text.char_indices() {
        token_stack.push(c);

        match state {
            State::Prefix => {
                if let Some(_) = token_stack.find("___") {
                    start_index = index - 2;
                    token_stack.clear();
                    state = State::Variable;
                }
            }
            State::Variable => {
                if let Some(var_index) = token_stack.find("___") {
                    end_index = index + 1;

                    if let Some(token) = token_stack.get(..var_index) {
                        // Check if it matches any of the template variable names
                        for (ref variable_name, ref variable_value) in template.vars().unwrap() {
                            let token: Token = TokenBuilder::new()
                                .default_case(TokenCase::SnakeCase)
                                .create(token);

                            let variable_name: Token= TokenBuilder::new()
                                .default_case(TokenCase::SnakeCase)
                                .create(variable_name);

                            if token == variable_name {
                                // We have a variable match, place it on to the list of changes
                                let value: String = TokenBuilder::new()
                                    .case(token.case())
                                    .create(&variable_value.value().unwrap())
                                    .to_string();

                                changes.push((start_index, end_index, value));
                            }
                        }
                    };

                    token_stack.clear();
                    state = State::Prefix;
                }
            }
        }
    }

    // For each match replace the text. Starting from the last match
    changes.reverse();

    for (start, end, val) in changes.iter() {
        text.replace_range(start..end, val);
    }

    text
}

///////////////////////////////////////////////////////////////////////////////
// Tests
// ----------------------------------------------------------------------------
#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    #[test]
    fn test_parse() {
        let template = Template::new(std::path::PathBuf::from(
            "/home/dalin/.config/tempr/c_header",
        ))
        .unwrap();

        assert_eq!("hello", _parse(&template, "___var_1___"));
    }

    #[test]
    fn test_parse_2() {
        let template = Template::new(std::path::PathBuf::from(
            "/home/dalin/.config/tempr/c_header",
        ))
        .unwrap();

        let mut path = std::env::current_dir().unwrap();
        path.push("Hello");

        let dir_transations = vec![DirTransaction { path: path }];
        let file_transations: Vec<FileTransaction> = Vec::new();

        //assert_eq!((dir_transations, file_transations), parse(&template));
    }

    #[test]
    fn test_to_case() {
        let words = ["hello", "there"];

        assert_eq!("HELLO_THERE".to_string(), to_case(Case::MacroCase, &words));
        assert_eq!("hello_there".to_string(), to_case(Case::SnakeCase, &words));
        assert_eq!("hello-there".to_string(), to_case(Case::LispCase, &words));
    }

    #[test]
    fn test_token_builder() {
    }
}

///////////////////////////////////////////////////////////////////////////////
// END OF FILE
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
//
//  File: bin.rs
//
//  Original Author: Dalin Riches (driches@ualberta.ca)
//
// Module Documentation -------------------------------------------------------
//! A tool for generating source files from a template.
//!
//! Searches for templates within the `~/.config/tempr` directory.
//!
//!
///////////////////////////////////////////////////////////////////////////////
//  Mods
// ----------------------------------------------------------------------------
mod parser;
mod template;

///////////////////////////////////////////////////////////////////////////////
//  Imports
// ----------------------------------------------------------------------------
use crate::parser::*;
use crate::template::Template;
use std::fs;
use std::io::Write;

///////////////////////////////////////////////////////////////////////////////
//  Exportes Types
// ----------------------------------------------------------------------------

///////////////////////////////////////////////////////////////////////////////
//  Internal Types
// ----------------------------------------------------------------------------

///////////////////////////////////////////////////////////////////////////////
// Implementation
// ----------------------------------------------------------------------------

/// Entry point of the program.
fn main() {
    // TODO: Argument Parsing

    let mut templates_dir = dirs::config_dir().unwrap();
    templates_dir.push("tempr");

    let templates_dir: &std::path::Path = templates_dir.as_path();
    let templates_entries = match fs::read_dir(templates_dir) {
        Ok(e) => e,
        Err(_) => {
            println!("No templates found!");
            return;
        }
    };

    let mut templates: Vec<Template> = Vec::new();

    // Read out all the templates within the templates directory
    println!("Select a template from the following list:");
    for entry in templates_entries {
        if let Ok(e) = entry {
            if let Some(t) = Template::new(e.path()) {
                println!("    {}: {}", templates.len(), t.name());
                templates.push(t);
            }
        }
    }
    print!(">>> ");
    std::io::stdout().flush().unwrap();

    let mut input = String::new();
    std::io::stdin().read_line(&mut input).unwrap();

    // Select the template
    let index = if let Ok(index) = input.trim().parse::<u32>() {
        if index >= templates.len() as u32 {
            println!("Invalid Index!");
            return;
        }
        index
    } else {
        println!("Invalid Index!");
        return;
    };

    // Get the values to use for each var
    if let Some(vars) = templates[index as usize].vars_mut() {
        for (name, var) in vars {
            println!("Enter value for \"{}\" (leave empty for default): ", name);
            print!(">>> ");
            std::io::stdout().flush().unwrap();

            let mut input = String::new();
            std::io::stdin().read_line(&mut input).unwrap();

            var.set(input.trim().to_string());
        }
    };

    // Determine Changes to be Made
    // ------------------------------------------------------------------------
    // * Determine directories that need to be created
    //      - Parse dir name for variables and replace with the variable value
    // * Determine files that need to be created/overwritten/appended
    //      - Parse file names for variables and replace with the variable
    //      value.
    let (dirs, files) = parser::parse(
        &templates[index as usize].path(),
        &std::env::current_dir().unwrap(),
        &templates[index as usize],
    );

    // Confirm transaction
    // ------------------------------------------------------------------------
    // Start by printing a summary of the transaction that is about to occur.
    if !dirs.is_empty() {
        println!("The following directories will be created:");

        for dir in dirs.iter() {
            println!("    {}", dir);
        }
    }

    if !files.is_empty() {
        println!("The following files will be created:");

        for file in files.iter() {
            println!("    {}", file);
        }
    }

    // Perform the Transaction
    // ------------------------------------------------------------------------
    //  * Create all directories that are needed
    //  * Create all files that are needed
    //      - Parse file content for variables
    // ------------------------------------------------------------------------
    for dir in dirs.iter() {
        dir.commit().unwrap();
    }

    for file in files.iter() {
        file.commit().unwrap();
    }
}

/// Displays a summary of the tranaction that is about to occur.
fn display_summary(template: &Template) {
    println!("Summary:");
    if let Some(vars) = template.vars() {
        println!("    Variables:");
        for (name, var) in vars {
            println!("        {} => \"{}\"", name, var.value().unwrap());
        }
    };
}

///////////////////////////////////////////////////////////////////////////////
// END OF FILE
///////////////////////////////////////////////////////////////////////////////

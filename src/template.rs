///////////////////////////////////////////////////////////////////////////////
//
//  File: template.rs
//
//  Original Author: Dalin Riches (driches@ualberta.ca)
//
// Module Documentation -------------------------------------------------------
//!
///////////////////////////////////////////////////////////////////////////////
//  Imports
// ----------------------------------------------------------------------------
use serde::Deserialize;
use std::collections::HashMap;
use std::error::Error;
use std::ffi::OsStr;
use std::fmt;
use std::fs::File;
use std::hash::Hash;
use std::io::BufReader;
use std::path::{Path, PathBuf};

///////////////////////////////////////////////////////////////////////////////
//  Exported Types
// ----------------------------------------------------------------------------
#[derive(Deserialize, Debug, PartialEq, Eq, Hash)]
pub enum VarCase {
    SnakeCase,
    CamelCase,
}

#[derive(Deserialize, Debug, PartialEq, Eq, Hash)]
pub struct Variable {
    value: Option<String>,
    case: Option<VarCase>,
}

pub struct Template {
    path: PathBuf,
    config: TemplateConfig,
}

#[derive(Deserialize, Debug)]
struct TemplateConfig {
    name: String,
    variables: Option<HashMap<String, Variable>>,
}

///////////////////////////////////////////////////////////////////////////////
//  Internal Types
// ----------------------------------------------------------------------------

///////////////////////////////////////////////////////////////////////////////
//  Internal Routines
// ----------------------------------------------------------------------------

///////////////////////////////////////////////////////////////////////////////
// Implementation
// ----------------------------------------------------------------------------
impl Template {
    /// Creates a new template struct.
    ///
    /// # Args
    /// * `path` - The path to the template folder. This folder must contain a
    ///            `template.json` file to be considered a valid template.
    pub fn new(path: PathBuf) -> Option<Self> {
        // Validate the path is a directory with a template.json file!
        if !Template::is_valid_template_dir(&path) {
            return None;
        }

        let mut config_path = path.clone();
        config_path.push("template.json");

        let config_file = File::open(config_path).unwrap();
        if let Ok(config) = TemplateConfig::load(&config_file) {
            return Some(Template {
                path: path,
                config: config,
            });
        }

        // template.json does not contain a valid config!
        None
    }

    /// Validates provided path is a directory that contains a `template.json`.
    ///
    /// This does not check that the `template.json` is valid.
    ///
    /// # Args
    /// * `path` - The path to the template directory.
    pub fn is_valid_template_dir(path: &PathBuf) -> bool {
        // Check path is a directory and exists
        if !path.exists() || !path.is_dir() {
            return false;
        }

        // Get the metadata for each entry and check if one of them is
        // template.json
        for entry in path.read_dir().unwrap() {
            if let Ok(entry) = entry {
                let entry_path = entry.path();

                if !entry_path.is_file() {
                    continue;
                }

                if OsStr::new("template.json") == entry_path.file_name().unwrap() {
                    // We have found a file named template.json.
                    return true;
                }
            }
        }

        false
    }

    pub fn name(&self) -> String {
        self.config.name.clone()
    }

    pub fn path(&self) -> &Path {
        self.path.as_path()
    }

    pub fn vars(&self) -> Option<impl Iterator<Item = (&String, &Variable)>> {
        if let Some(vars) = &self.config.variables {
            Some(vars.iter())
        } else {
            None
        }
    }

    pub fn vars_mut(&mut self) -> Option<impl Iterator<Item = (&String, &mut Variable)>> {
        if let Some(vars) = &mut self.config.variables {
            Some(vars.iter_mut())
        } else {
            None
        }
    }

}

impl fmt::Display for Template {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        writeln!(f, "Template: \"{}\"", self.config.name)?;
        writeln!(f, "    Path: {:?}", self.path)?;

        if let Some(vars) = &self.config.variables {
            writeln!(f, "    Config:")?;
            writeln!(f, "        Vars:")?;

            for var in vars {
                writeln!(f, "            {} => {{{}}}", var.0, var.1)?;
            }
        }

        Ok(())
    }
}

impl Variable {
    pub fn value(&self) -> Option<String> {
        self.value.clone()
    }

    pub fn set(&mut self, value: String) {
        self.value = Some(value);
    }
}

impl fmt::Display for Variable {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        if let Some(value) = &self.value {
            write!(f, ", value: {}", value)?;
        }

        if let Some(case) = &self.case {
            write!(f, ", case: {}", case)?;
        }

        Ok(())
    }
}

impl fmt::Display for VarCase {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            VarCase::SnakeCase => write!(f, "snake_case"),
            VarCase::CamelCase => write!(f, "camel_case"),
        }
    }
}

impl TemplateConfig {
    fn load(json: &File) -> Result<TemplateConfig, Box<dyn Error>> {
        let json_reader = BufReader::new(json);
        let config = serde_json::from_reader(json_reader)?;

        Ok(config)
    }
}

///////////////////////////////////////////////////////////////////////////////
// END OF FILE
///////////////////////////////////////////////////////////////////////////////
